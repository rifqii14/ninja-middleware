<?php



namespace App\Libraries;

use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class MainApiLibrary {

	public function __construct() {
		$this->main_api_url = 'http://ninja-api.fath';
		$this->method = "";
	}

	public function insert_shipper_master($param){
		$this->action = "insert_shipper_master";
		$end_point = "/api/main/insert_shipper_master";
        $params = array(
			"shipper_id"  		=>  $param['shipper_id'],
			"shipper_name"  	=>  $param['shipper_name'],
			"client_id"  		=>  $param['client_id'],
			"client_secret"  	=>  $param['client_secret']
        );
		return json_decode((string) $this->execute($this->main_api_url.$end_point, json_encode($params)));
	}

	public function get_shipper_master($param){
		$this->action = "get_shipper_master";
		$end_point = "/api/main/get_shipper_master";
        $params = array(
			"shipper_id"  		=>  $param['shipper_id'],
			"shipper_name"  	=>  $param['shipper_name'],
			"client_id"  		=>  $param['client_id'],
			"client_secret"  	=>  $param['client_secret']
        );
		return json_decode((string) $this->execute($this->main_api_url.$end_point, json_encode($params)));
	}

	public function insert_driver($param){
		$this->action = "insert_driver";
		$end_point = "/api/main/insert_driver";
        $params = array(
			"shipper_id"  		=>  $param['shipper_id'],
			"order_id"  		=>  $param['order_id'],
			"tracking_id"  		=>  $param['tracking_id'],
			"driver_name"  		=>  $param['driver_name']
        );
		return json_decode((string) $this->execute($this->main_api_url.$end_point, json_encode($params)));
    }

    public function insert_batch_driver($request)
    {
        $this->action = "insert_batch_driver";
        $end_point = "/api/main/insert_batch_driver";

        return json_decode((string)
            $this->execute(
                $this->main_api_url . $end_point, json_encode($request))
        );
    }

	public function get_driver($param){
		$this->action = "get_driver";
		$end_point = "/api/main/get_driver";
        $params = array(
			"shipper_id"  		=>  $param['shipper_id'],
			"order_id"  		=>  $param['order_id'],
			"tracking_id"  		=>  $param['tracking_id'],
			"driver_name"  		=>  $param['driver_name']
        );
		return json_decode((string) $this->execute($this->main_api_url.$end_point, json_encode($params)));
	}

	public function get_webhook($param){
		$this->action = "get_webhook";
		$end_point = "/api/main/get_webhook";
        $params = array(
			"id_log" => $param['id_log'],
			"date" =>  $param['date'],
			"shipper" =>  $param['shipper'],
			"tracking_number" =>  $param['tracking_number']
        );
		return json_decode((string) $this->execute($this->main_api_url.$end_point, json_encode($params)));
	}

	public function on_delivery($param){
		$this->action = "on_delivery";
		$end_point = "/api/on_delivery";

		return json_decode((string) $this->execute($this->main_api_url.$end_point, json_encode($param)));
	}

	public function successful_delivery($param){
		$this->action = "successful_delivery";
		$end_point = "/api/successful_delivery";

		return json_decode((string) $this->execute($this->main_api_url.$end_point, json_encode($param)));
	}

	public function cancel($param){
		$this->action = "cancel";
		$end_point = "/api/cancel";

		return json_decode((string) $this->execute($this->main_api_url.$end_point, json_encode($param)));
	}

	public function return_to_sender_trigger($param){
		$this->action = "return_to_sender_trigger";
		$end_point = "/api/return_to_sender_trigger";

		return json_decode((string) $this->execute($this->main_api_url.$end_point, json_encode($param)));
	}

	/**
	 * Base for call service
     * @param string $url               destination url serice
     * @param string $data              data send to service
	 * @return object $server_output    json decoded of response from service API
	 */
	public function execute($url, $data) {
		Log::info("<<<<<<<<<< Start Call API Service >>>>>>>>>>");
		Log::info('[header '.$this->action.'] : '.json_encode(array(
			'Content-Type: application/json'
		)));
		Log::info('[Request '.$this->action.' API service] : '.$data);
		$start = microtime(true);
    	$curl = curl_init();
		curl_setopt($curl, CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
				'X-Username: '.Session::get('username')
            )
        );
		if($this->method == "GET"){
			curl_setopt($curl, CURLOPT_HTTPGET, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		}else{
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		}
    	curl_setopt($curl, CURLOPT_URL, $url);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$server_output = curl_exec($curl);
		$end = microtime(true);
		$this->response_time = $end - $start;
		$this->http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        //cek tidak ada balasan

        if (empty($server_output)) {
            curl_close($curl);
			$response = [
				"response_code" => '99',
				"response_desc" => 'Gagal Connect ke API service',
            ];
            Log::info('[Response '.$this->action.' API service] : '.json_encode($response));
			return json_encode($response);
        }

        //check http code response
        if(curl_getinfo($curl, CURLINFO_HTTP_CODE) != 200)
        {
			$response = [
				"response_code" => '99',
				"response_desc" => curl_getinfo($curl, CURLINFO_HTTP_CODE)." - Terjadi kesalahan pada sistem",
            ];
            Log::info('[Response '.$this->action.' API service] : '.json_encode($response));
			curl_close($curl);
			return json_encode($response);
        }

        //return jika http 200
        if(curl_getinfo($curl, CURLINFO_HTTP_CODE) == 200)
        {
			curl_close($curl);
            Log::info('[Response '.$this->action.' API service] : '.$server_output);
			return $server_output;
        }

	}
}
