<?php

namespace App\Http\Livewire;

use App\Libraries\MainApiLibrary;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Livewire\Component;

class ShipperMaster extends Component
{
    protected $main_api;
    public $search, $isSearch = false;
    public function render()
    {
        $this->main_api = new MainApiLibrary();
        $param = array(
            "shipper_id" => '',
            "shipper_name" => '',
            "client_id" => '',
            "client_secret" => '',
        );

        $get = $this->main_api->get_shipper_master($param);
        $data = $this->paginate($get->response_data);
        $this->isSearch = false;
        if($this->search != '') {
            $this->isSearch = true;
            foreach ($get->response_data as $key => $value) { $data = [];
                if (
                    strpos(\strtolower($value->shipper_name), \strtolower($this->search)) !== false
                    // \preg_match('/'.$value->shipper_name.'/i', $this->search)
                ) { $dataShiper[] = $value; } }
            if(isset($dataShiper)) { $data = $this->paginate($dataShiper); }
        }
        return view('livewire.shipper-master', ['data' => $data]);
    }

    public function paginate($items, $perPage = 15) {
        $pageStart = \Request::get('page', 1);
        // Start displaying items from this number;
        $offSet = ($pageStart * $perPage) - $perPage;
        // Get only the items you need using array_slice
        $itemsForCurrentPage = array_slice($items, $offSet, $perPage, true);
        return new LengthAwarePaginator($itemsForCurrentPage, count($items), $perPage, Paginator::resolveCurrentPage(), array('path' => Paginator::resolveCurrentPath()));
    }
}
