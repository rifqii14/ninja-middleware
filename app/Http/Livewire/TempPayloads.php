<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class TempPayloads extends Component
{
    use WithPagination;

    public $search;
    public function render()
    {
        $data = DB::table('temp_payloads')
            ->where('tracking_id', 'LIKE', '%'.$this->search.'%')
            ->paginate(10);
        return view('livewire.temp-payloads',['data' => $data]);
    }
}
