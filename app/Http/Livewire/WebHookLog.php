<?php

namespace App\Http\Livewire;

use App\Libraries\MainApiLibrary;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Livewire\Component;

class WebhookLog extends Component
{
    protected $main_api;
    public $search, $eventSelect = null, $isSearch = false;
    public function render()
    {
        $this->main_api = new MainApiLibrary();
        $param = array(
            "id_log" => "",
            "date" =>  "",
            "shipper" =>  "",
            "tracking_number" =>  ""
        );

        $get = $this->main_api->get_webhook($param);
        $data = $this->paginate($get->response_data);
        $this->isSearch = false;
        if($this->search != '') {
            $this->isSearch = true;
            foreach ($get->response_data as $key => $value) { $data = [];
                if (
                    strpos(strtolower($value->tracking_id), strtolower($this->search)) !== false
                    // \preg_match('/'.$value->tracking_id.'/i', $this->search)
                ) { $dataTracking[] = $value; } }
            if(isset($dataTracking)) {
                $data = $dataTracking; } }

        if ($this->eventSelect != null) {
            foreach ($get->response_data as $key => $value) { $data = [];
                if ($value->event_type == $this->eventSelect) { $dataEvent[] = $value; } }
            if(isset($dataEvent)) {
                $data = $this->paginate($dataEvent); } }

        return view('livewire.webhook-log', ['data' => $data]);
    }

    public function paginate($items, $perPage = 15) {
        $pageStart = \Request::get('page', 1);
        // Start displaying items from this number;
        $offSet = ($pageStart * $perPage) - $perPage;
        // Get only the items you need using array_slice
        $itemsForCurrentPage = array_slice($items, $offSet, $perPage, true);
        return new LengthAwarePaginator($itemsForCurrentPage, count($items), $perPage, Paginator::resolveCurrentPage(), array('path' => Paginator::resolveCurrentPath()));
    }
}
