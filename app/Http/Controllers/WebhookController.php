<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Libraries\MainAPILibrary;
use Illuminate\Support\Facades\Log;

class WebhookController extends Controller
{
    public $viewDir = "webhook";


    public function __construct() {
        $this->middleware('auth');
        $this->main_api = new MainAPILibrary();
    }

    protected function view($view, $data = []) {
        return view($this->viewDir . "." . $view, $data);
    }

    public function index() {
        return $this->view('webhook');
    }

    public function webhookbyId(Request $request) {
        $param = array(
            "id_log" => $request->input('id_log'),
            "date" =>  "",
            "shipper" =>  "",
            "tracking_number" =>  ""
        );

        $get = $this->main_api->get_webhook($param);

        return json_encode($get);
    }

    public function on_delivery(Request $request){
        $param = $request->input('body');

        $insert = $this->main_api->on_delivery($param);

        $data = $insert;


        return response()->json(array(
            'response' => $data
        ));
    }

    public function cancel(Request $request){
        $param = $request->input('body');

        $insert = $this->main_api->cancel($param);

        $data = $insert;


        return response()->json(array(
            'response' => $data
        ));
    }

    public function successful_delivery(Request $request){
        $param = $request->input('body');

        $insert = $this->main_api->successful_delivery($param);

        $data = $insert;


        return response()->json(array(
            'response' => $data
        ));
    }

    public function return_to_sender_trigger(Request $request){
        $param = $request->input('body');

        $insert = $this->main_api->return_to_sender_trigger($param);

        $data = $insert;


        return response()->json(array(
            'response' => $data
        ));
    }
}
