<?php

namespace App\Http\Controllers;

use App\Exports\DriverExport;
use Illuminate\Http\Request;
use App\Libraries\MainAPILibrary;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Excel;

class DriverController extends Controller
{
    public $viewDir = "driver";


    public function __construct() {
        $this->middleware('auth');
        $this->main_api = new MainAPILibrary();
    }

    protected function view($view, $data = []) {
        return view($this->viewDir . "." . $view, $data);
    }

    public function index() {
        return $this->view('driver');
    }

    public function insert() {
        return $this->view('form_driver');
    }

    public function update(Request $request) {
        try {
            DB::beginTransaction();
            DB::table('driver')->where('id', $request->id)
            ->update([
                'shipper_id' => $request->shipper_id,
                'order_id' => $request->order_id,
                'tracking_id' => $request->tracking_id,
                'driver_name' => $request->driver_name,
            ]);
            DB::commit();
            return redirect()->back()->with('success', 'Data '.$request->shipper_id.' berhasil diubah');
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function delete(Request $request)
    {
        try {
            DB::beginTransaction();
            DB::table('driver')->where('id', $request->id)->delete();
            DB::commit();
            return redirect()->back()->with('success', 'Data ' . $request->shipper_id . ' berhasil dihapus');
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function insert_process(Request $request){
        $param = array(
            "shipper_id"  	    =>  $request->input('shipper_id'),
            "order_id"  	=>  $request->input('order_id'),
            "tracking_id"  	    =>  $request->input('tracking_id'),
            "driver_name"  	    =>  $request->input('driver_name'),
        );

        $insert = $this->main_api->insert_driver($param);

        $data = $insert;

        if ($data->response_code !== "00") {
            $response = [
                "response_code" => $data->response_code,
                "response_desc" => $data->response_desc,
            ];
            Log::info('[Response] : ' . json_encode($response));
        }

        return response()->json(array(
            'response_code' => $data->response_code,
            'response_desc' => $data->response_desc,
        ));
    }

    public function insert_batch(Request $request) {
        return $this->main_api->insert_batch_driver($request);
    }

    public function template() {
        return Excel::download(new DriverExport, 'driver_template.xlsx');
    }
}
