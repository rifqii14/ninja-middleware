<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Libraries\MainAPILibrary;
use Illuminate\Support\Facades\Log;

class ShipperMasterController extends Controller
{
    public $viewDir = "shipper_master";


    public function __construct() {
        $this->middleware('auth');
        $this->main_api = new MainAPILibrary();
    }

    protected function view($view, $data = []) {
        return view($this->viewDir . "." . $view, $data);
    }

    public function index() {
        return $this->view('shipper_master');
    }

    public function insert() {
        return $this->view('form_shipper_master');
    }

    public function insert_process(Request $request){
        $param = array(
            "shipper_id"  	    =>  $request->input('shipper_id'),
            "shipper_name"  	=>  $request->input('shipper_name'),
            "client_id"  	    =>  $request->input('client_id'),
            "client_secret"  	    =>  $request->input('client_secret'),
        );

        $insert = $this->main_api->insert_shipper_master($param);

        $data = $insert;

        if ($data->response_code !== "00") {
            $response = [
                "response_code" => $data->response_code,
                "response_desc" => $data->response_desc,
            ];
            Log::info('[Response] : ' . json_encode($response));
        }

        return response()->json(array(
            'response_code' => $data->response_code,
            'response_desc' => $data->response_desc,
        ));
    }
}
