<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return redirect('/login'); });

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

$router->group(['prefix' => 'shipper-master'], function ($app) {
    $app->match(['post', 'get'], '/', ['uses' => 'ShipperMasterController@index'])->name('shipper-master');
    $app->match(['post', 'get'], '/insert', ['uses' => 'ShipperMasterController@insert']);
    $app->match(['post', 'get'], '/insert-process', ['uses' => 'ShipperMasterController@insert_process']);
});

$router->group(['prefix' => 'driver'], function ($app) {
    $app->match(['post', 'get'], '/', ['uses' => 'DriverController@index'])->name('driver');
    $app->match(['post', 'get'], '/insert', ['uses' => 'DriverController@insert']);
    $app->match(['post', 'get'], '/update', ['uses' => 'DriverController@update']);
    $app->match(['post', 'get'], '/delete', ['uses' => 'DriverController@delete']);
    $app->match(['post', 'get'], '/template', ['uses' => 'DriverController@template']);
    $app->match(['post', 'get'], '/insert-process', ['uses' => 'DriverController@insert_process']);
    $app->match(['post', 'get'], '/insert-batch', ['uses' => 'DriverController@insert_batch']);
});

$router->group(['prefix' => 'webhook-log'], function ($app) {
    $app->match(['post', 'get'], '/', ['uses' => 'WebhookController@index'])->name('webhook-log');
    $app->match(['post', 'get'], '/id', ['uses' => 'WebhookController@webhookbyId']);

    $app->match(['post', 'get'], '/refire/on_delivery', ['uses' => 'WebhookController@on_delivery']);
    $app->match(['post', 'get'], '/refire/successful_delivery', ['uses' => 'WebhookController@successful_delivery']);
    $app->match(['post', 'get'], '/refire/cancel', ['uses' => 'WebhookController@cancel']);
    $app->match(['post', 'get'], '/refire/return_to_sender_trigger', ['uses' => 'WebhookController@return_to_sender_trigger']);
});

Route::get('/livewire/message/{pages}', function() {
    $pages = Request::segment(3);
    $url = url('/') . '/' . $pages . '?page=' . (isset($_GET['page']) ? $_GET['page'] : '');
    return redirect($url);
});
Route::get('/temp-payloads', function () { return view('temp-payloads.temp-payloads'); })->name('temp-payloads');

Route::get('/profile', 'ProfileController@index')->name('profile');
Route::put('/profile', 'ProfileController@update')->name('profile.update');

Route::get('/about', function () { return view('about'); })->name('about');
