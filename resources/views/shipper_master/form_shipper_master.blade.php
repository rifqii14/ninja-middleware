@extends('layouts.admin')

@section('main-content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ __('Insert Shipper Master') }}</h1>

    @if (session('success'))
        <div class="alert alert-success border-left-success alert-dismissible fade show" role="alert">
            {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger border-left-danger" role="alert">
            <ul class="pl-4 my-2">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">

        <div class="col-lg-12 order-lg-1">

            <div class="card shadow mb-4">

                {{-- <div class="card-header py-3" style="border:none;">
                    <h6 class="m-0 font-weight-bold text-primary">My Account</h6>
                </div> --}}

                <div class="card-body">

                    <form method="POST" onsubmit="insert_shipper_master(event)" autocomplete="off">
                        <div class="pl-lg-4">

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group focused">
                                        <label class="form-control-label" for="name">Shipper ID<span class="small text-danger">*</span></label>
                                        <input type="text" id="shipper_id" class="form-control" name="shipper_id">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group focused">
                                        <label class="form-control-label" for="name">Shipper Name<span class="small text-danger">*</span></label>
                                        <input type="text" id="shipper_name" class="form-control" name="shipper_name">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group focused">
                                        <label class="form-control-label" for="name">Client ID<span class="small text-danger">*</span></label>
                                        <input type="text" id="client_id" class="form-control" name="client_id">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group focused">
                                        <label class="form-control-label" for="name">Client Secret<span class="small text-danger">*</span></label>
                                        <input type="text" id="client_secret" class="form-control" name="client_secret">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Button -->
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col text-left">
                                    <button type="submit" class="btn btn-success">SIMPAN</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        function insert_shipper_master(e) {
            let url = "{{ url('/shipper-master/insert-process') }}";
            e.preventDefault();
            $.ajax({
                url: url,
                type: "POST",
                timeout: 180000,
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'shipper_id': $('#shipper_id').val(),
                    'shipper_name':$('#shipper_name').val(),
                    'client_id':$('#client_id').val(),
                    'client_secret':$('#client_secret').val()
                },
                success: function(response) {
                    if (response.response_code == "00") {
                        alert('Berhasil')
                        document.location.href = '{{ url('shipper-master') }}';
                    } else {
                        alert('Gagal <br>'+response.response_desc)
                        return false;
                    }
                },
                error: function(response) {
                    console.log(response);
                }
            });
        }
    </script>

@endsection


