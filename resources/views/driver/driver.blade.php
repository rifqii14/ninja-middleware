@extends('layouts.admin')

@section('main-content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ 'Driver' }}</h1>

    @if (session('success'))
    <div class="alert alert-success border-left-success alert-dismissible fade show" role="alert">
        {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    @if (session('status'))
        <div class="alert alert-success border-left-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex" style="border:none; justify-content: end;">
            <h6 class="mx-1 font-weight-bold text-right">
                <a href="{{ url('/').'/driver/template' }}" class="btn btn-white text-dark">
                    <i class="fas fa-file-download mx-1"></i> Template CSV
                </a>
            </h6>
            <h6 class="mx-1 font-weight-bold text-right">
                <form
                action="http://ninja-api.fath/api/main/insert_batch_driver"
                {{--  action="{{ url('/').'/driver/insert-batch' }}"   --}}
                method="POST" enctype="multipart/form-data">
                @csrf
                    <button class="btn btn-success text-white" type="button" data-toggle="modal" data-target="#exampleModal">
                        <i class="fas fa-file-excel mx-1"></i> Upload CSV
                    </button>
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header" style="border: none;">
                                    <h5 class="modal-title" id="exampleModalLabel">Upload CSV</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body pb-0 text-left">
                                    <input type="file" name="file" id="file" class="form-file">
                                </div>
                                <div class="modal-footer" style="border: none;">
                                    <button type="submit" class="btn btn-success"> UPLOAD </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </h6>
            <h6 class="mx-1 font-weight-bold text-right">
                <a href="{{ url('/') }}/driver/insert" class="btn btn-success text-white">
                    <i class="fas fa-plus mx-1"></i> Tambah Data
                </a>
            </h6>
        </div>

        <div class="card-body">
            @livewire('driver')
        </div>
    </div>
@endsection
