@extends('layouts.admin')

@section('main-content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ 'Webhook Log' }}</h1>

    @if (session('success'))
    <div class="alert alert-success border-left-success alert-dismissible fade show" role="alert">
        {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    @if (session('status'))
        <div class="alert alert-success border-left-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <style>
        .scrollable
        {
            width:100%;
            height: 100px;
            margin: 0;
            padding: 0;
            overflow-y: scroll
        }
    </style>

    <div class="card shadow mb-4">
        {{-- <div class="card-header py-3" style="border:none;">
            <h6 class="m-0 font-weight-bold text-primary"><a href="{{ url('/') }}/shipper-master/insert">Tambah Data</a></h6>
        </div> --}}

        <div class="card-body">
            @livewire('webhook-log')
        </div>

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-borderless table-striped" id="dataTable"  cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Response Body Webhook</th>
                                    <th>Response Result Webhook</th>
                                    <th>Response Code Webhook</th>
                                    <th>URL</th>
                                </tr>
                            </thead>
                                <tbody>
                                    <tr>
                                        <td><pre id="rb_webhook" ></pre></td>
                                        <td><pre id="rr_webhook" ></pre></td>
                                        <td><pre id="rc_webhook" ></pre></td>
                                        <td><pre id="url_webhook" ></pre></td>
                                    </tr>
                                </tbody>
                        </table>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-borderless table-striped" id="dataTable"  cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Response Body Client</th>
                                    <th>Response Result Client</th>
                                    <th>Response Code Client</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><pre id="rb_client"></pre></td>
                                    <td><pre id="rr_client"></pre></td>
                                    <td><pre id="rc_client"></pre></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer" style="border: none;">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="refire">Refire</button>
                </div>
            </div>
            </div>
        </div>
    </div>
    <script>
        function get_webhook_id(e,id) {
            let url = "{{ url('/webhook-log/id') }}";
            e.preventDefault();
            $.ajax({
                url: url,
                type: "POST",
                timeout: 180000,
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'id_log': id,
                },
                success: function(response) {
                    if (response.response_code == "00") {
                        $('#rb_webhook').text(JSON.stringify(JSON.parse(response.response_data[0].response_body_webhook), null, 2));
                        $('#rr_webhook').text(JSON.stringify(JSON.parse(response.response_data[0].response_result_webhook), null, 2));
                        $('#rc_webhook').text(JSON.stringify(JSON.parse(response.response_data[0].response_code_webhook), null, 2));

                        if(response.response_data[0].response_body_client !=""){
                            $('#rb_client').text(JSON.stringify(JSON.parse(response.response_data[0].response_body_client), null, 2));
                        }

                        if(response.response_data[0].response_result_client !=""){
                            $('#rr_client').text(JSON.stringify(JSON.parse(response.response_data[0].response_result_client), null, 2));
                        }

                        if(response.response_data[0].response_code_client !=""){
                            $('#rc_client').text(JSON.stringify(JSON.parse(response.response_data[0].response_code_client), null, 2));
                        }

                        $('#url_webhook').text(response.response_data[0].url);

                        $('#refire').attr('onClick','refire(event,'+response.response_data[0].response_body_webhook+')');
                        $('#refire').attr('data-event',response.response_data[0].event_type)

                        $("#exampleModal").modal();
                    } else {
                        console.log(response)
                    }
                },
                error: function(response) {
                    console.log(response);
                }
            });
        }

        function refire(e,body) {
            let url = "{{ url('/webhook-log/refire/') }}/"+$('#refire').data('event');
            e.preventDefault();
            $.ajax({
                url: url,
                type: "POST",
                timeout: 180000,
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'body': body,
                },
                success: function(response) {

                    alert('Refire berhasil')
                    document.location.href = '{{ url('webhook-log') }}';
                    // if (response.response_code == "00") {
                    //     $('#rb_webhook').text(JSON.stringify(JSON.parse(response.response_data[0].response_body_webhook), null, 2));
                    //     $('#rr_webhook').text(JSON.stringify(JSON.parse(response.response_data[0].response_result_webhook), null, 2));
                    //     $('#rc_webhook').text(JSON.stringify(JSON.parse(response.response_data[0].response_code_webhook), null, 2));

                    //     $('#rb_client').text(JSON.stringify(JSON.parse(response.response_data[0].response_body_client), null, 2));
                    //     $('#rr_client').text(JSON.stringify(JSON.parse(response.response_data[0].response_body_client), null, 2));
                    //     $('#rc_client').text(JSON.stringify(JSON.parse(response.response_data[0].response_body_client), null, 2));

                    //     $('#url_webhook').text(response.response_data[0].url);
                    //     $("#exampleModal").modal();
                    // } else {
                    //     console.log(response)
                    // }
                },
                error: function(response) {
                    console.log(response);
                }
            });
        }
    </script>
@endsection
