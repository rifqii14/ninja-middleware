@extends('layouts.admin')

@section('main-content')
<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">{{ 'Temp Payloads' }}</h1>

@if (session('success'))
    <div class="alert alert-success border-left-success alert-dismissible fade show" role="alert">
        {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

@if (session('status'))
    <div class="alert alert-success border-left-success" role="alert">
        {{ session('status') }}
    </div>
@endif

<style>
    .scrollable {
        width: 100%;
        height: 100px;
        margin: 0;
        padding: 0;
        overflow-y: scroll
    }
</style>

<div class="card shadow mb-4">
    {{-- <div class="card-header py-3" style="border:none;">
            <h6 class="m-0 font-weight-bold text-primary"><a href="{{ url('/') }}/shipper-master/insert">Tambah Data</a></h6>
    </div> --}}

    <div class="card-body">
        @livewire('temp-payloads')
    </div>

</div>
@endsection

