<div class="table-responsive">
    <input type="text" wire:model="search" placeholder="Cari disini.." class="form-control mb-4">
    <table class="table table-striped table-borderless" id="dataTable">
        <thead>
            <tr>
                <th>Shipper ID</th>
                <th>Shipper Name</th>
                <th>Client ID</th>
                <th>Client Secret</th>
            </tr>
        </thead>
        <tbody>
            @forelse($data as $dataShipper)
                <tr>
                    <td>{{$dataShipper->shipper_id}}</td>
                    <td>{{$dataShipper->shipper_name}}</td>
                    <td>{{$dataShipper->client_id}}</td>
                    <td>{{$dataShipper->client_secret}}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="100" class="text-center"> Data tidak ditemukan</td>
                </tr>
            @endforelse
        </tbody>
        @if(!$isSearch)
            @if(count($data) > 0)
            <tfoot>
                <tr>
                    <td colspan="100" class="text-left pt-4" style="justify-content: end;">
                        <div class="mx-auto text-left" style="width: max-content; float: right;">
                            {{ $data->links() }}
                        </div>
                    </td>
                </tr>
            </tfoot>
            @endif
        @endif
    </table>
</div>
