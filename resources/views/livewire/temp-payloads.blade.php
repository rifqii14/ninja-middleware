<div class="table-responsive">
    <input type="text" wire:model="search" placeholder="Cari disini.." class="form-control mb-4">
    <table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Shipper ID</th>
                <th>Status</th>
                <th>Tracking ID</th>
                <th>Time Stamp</th>
                <th>Comment</th>
                <th>Payload Status/Type</th>
            </tr>
        </thead>
        <tbody>
            @forelse($data as $dataTemp)
                <tr>
                    <td>{{$dataTemp->shipper_id}}</td>
                    <td>{{$dataTemp->status}}</td>
                    <td>{{$dataTemp->tracking_id}}</td>
                    <td>{{ $dataTemp->timestamp }}</td>
                    <td>{{ $dataTemp->comments }}</td>
                    <td>{{ $dataTemp->payload_status ?? '-' }} / {{ $dataTemp->payload_type ?? '-' }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="100" class="text-center"> Data tidak ditemukan</td>
                </tr>
            @endforelse
        </tbody>
        @if(count($data) > 0)
            <tfoot>
                <tr>
                    <td colspan="100" class="text-left pt-4" style="justify-content: end;">
                        <div class="mx-auto text-left" style="width: max-content; float: right;">
                            {{ $data->links() }}
                        </div>
                    </td>
                </tr>
            </tfoot>
        @endif
    </table>
</div>
