<div class="table-responsive">
    <input type="text" wire:model="search" placeholder="Cari disini.." class="form-control mb-4">
    <table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Shipper ID</th>
                <th>Tracking ID</th>
                <th>Driver Name</th>
                <th class="text-center">Opsi</th>
            </tr>
        </thead>
        <tbody>
            @forelse($data as $dataDriver)
                <tr>
                    <td>{{$dataDriver->shipper_id}}</td>
                    <td>{{$dataDriver->tracking_id}}</td>
                    <td>{{$dataDriver->driver_name}}</td>
                    <td>
                        <div class="d-flex text-center" style="justify-content: center;">
                            <button class="btn btn-info mr-2" data-toggle="modal" data-target="#editDriver{{ $dataDriver->id }}">EDIT</button>
                            <!-- Modal -->
                            <form class="modal fade" id="editDriver{{ $dataDriver->id }}" tabindex="-1" role="dialog"
                            aria-labelledby="editDriver{{ $dataDriver->id }}Title" aria-hidden="true" action="{{ url('/').'/driver/update' }}" method="POST">
                                @csrf
                                <input type="hidden" name="id" value="{{ $dataDriver->id }}">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header" style="border: none;">
                                            <h5 class="modal-title" id="exampleModalLongTitle">
                                                <small>SHIPPER ID.</small> {{ $dataDriver->shipper_id }}
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-left">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group focused">
                                                        <label class="form-control-label" for="name">Shipper ID<span class="small text-danger">*</span></label>
                                                        <input type="text" id="shipper_id" class="form-control" name="shipper_id" value="{{ $dataDriver->shipper_id }}">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group focused">
                                                        <label class="form-control-label" for="name">Order ID<span class="small text-danger">*</span></label>
                                                        <input type="text" id="order_id" class="form-control" name="order_id" value="{{ $dataDriver->order_id }}">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group focused">
                                                        <label class="form-control-label" for="name">Tracking ID<span class="small text-danger">*</span></label>
                                                        <input type="text" id="tracking_id" class="form-control" name="tracking_id" value="{{ $dataDriver->tracking_id }}">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group focused">
                                                        <label class="form-control-label" for="name">Driver Name<span class="small text-danger">*</span></label>
                                                        <input type="text" id="driver_name" class="form-control" name="driver_name" value="{{ $dataDriver->driver_name }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer" style="border: none;">
                                            <button type="submit" class="btn btn-success">SIMPAN</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <button class="btn btn-danger mr-2" data-toggle="modal"
                            data-target="#deleteDriver{{ $dataDriver->id }}">DELETE</button>
                            <!-- Modal -->
                            <form class="modal fade" id="deleteDriver{{ $dataDriver->id }}" tabindex="-1" role="dialog"
                            aria-labelledby="deleteDriver{{ $dataDriver->id }}Title" aria-hidden="true" action="{{ url('/').'/driver/delete' }}" method="POST">
                            @csrf
                                <input type="hidden" name="id" value="{{ $dataDriver->id }}">
                                <input type="hidden" name="shipper_id" value="{{ $dataDriver->shipper_id }}">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header" style="border: none;">
                                            <h5 class="modal-title" id="exampleModalLongTitle">
                                                <small>SHIPPER ID.</small> {{ $dataDriver->shipper_id }}
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-center">
                                            <p> Yakin menghapus data ini? </p>
                                        </div>
                                        <div class="modal-footer" style="border: none;">
                                            <button type="submit" class="btn btn-danger">HAPUS</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="100" class="text-center"> Data tidak ditemukan</td>
                </tr>
            @endforelse
        </tbody>
        @if(!$isSearch)
            @if(count($data) > 0)
            <tfoot>
                <tr>
                    <td colspan="100" class="text-left pt-4" style="justify-content: end;">
                        <div class="mx-auto text-left" style="width: max-content; float: right;">
                            {{ $data->links() }}
                        </div>
                    </td>
                </tr>
            </tfoot>
            @endif
        @endif
    </table>
</div>
