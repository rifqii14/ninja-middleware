<div class="table-responsive" style="overflow-x: hidden;">
    <div class="row">
        <div class="col-sm-12 col-md-10">
            <input type="text" wire:model="search" placeholder="Cari disini.." class="form-control mb-4">
        </div>
        <div class="col-sm-12 col-md-2">
            <select name="" id="" wire:model="eventSelect" class="form-control">
                <option value=""> -- event type -- </option>
                <option value="on_delivery"> on_delivery </option>
                <option value="successful_delivery"> successful_delivery </option>
                <option value="cancel"> cancel </option>
            <option value="return_to_sender"> return_to_sender </option>
            </select>
        </div>
    </div>
    <table class="table table-borderless table-striped" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Shipper</th>
                <th>Date</th>
                <th>Tracking ID</th>
                <th>Event Type</th>
                <th>Detail</th>
            </tr>
        </thead>
        <tbody>
            @forelse($data as $dataWebhook)
                <tr>
                    <td>{{ $dataWebhook->shipper }}</td>
                    <td>{{ Carbon\Carbon::create($dataWebhook->date)->format('l, d M Y H:i:s') }}</td>
                    <td>{{ $dataWebhook->tracking_id }}</td>
                    <td>{{ $dataWebhook->event_type }}</td>
                    <td>
                        <button type="button" class="btn btn-primary"
                        onclick="get_webhook_id(event,{{$dataWebhook->id_log}})" id="detail">
                            Detail
                        </button>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="100" class="text-center pt-3">
                        <div class="mx-auto text-center" style="width: max-content;">
                            <span>Data tidak ditemukan</span>
                        </div>
                    </td>
                </tr>
            @endforelse
        </tbody>
        @if(!$isSearch)
            @if(count($data) > 0)
                <tfoot>
                    <tr>
                        <td colspan="100" class="text-left pt-4" style="justify-content: end;">
                            <div class="mx-auto text-left" style="width: max-content; float: right;">
                                {{ $data->links() }}
                            </div>
                        </td>
                    </tr>
                </tfoot>
            @endif
        @endif
    </table>
</div>
